//
// Exercise Details:
// Build an amortization schedule program using Java. 
// 
// The program should prompt the user for
//		the amount he or she is borrowing,
//		the annual percentage rate used to repay the loan,
//		the term, in years, over which the loan is repaid.  
// 
// The output should include:
//		The first column identifies the payment number.
//		The second column contains the amount of the payment.
//		The third column shows the amount paid to interest.
//		The fourth column has the current balance, the total payment amount and the interest paid fields.
// 
// Use appropriate variable names and comments.  You choose how to display the output (i.e. Web, console).  
// Amortization Formula
// This will get you your monthly payment.  Will need to update to Java.
// M = P * (J / (1 - (Math.pow(1/(1 + J), N))));
// 
// Where:
// P = Principal
// I = Interest
// J = Monthly Interest in decimal form:  I / (12 * 100)
// N = Number of months of loan
// M = Monthly Payment Amount
// 
// To create the amortization table, create a loop in your program and follow these steps:
// 1.      Calculate H = P x J, this is your current monthly interest
// 2.      Calculate C = M - H, this is your monthly payment minus your monthly interest, so it is the amount of principal you pay for that month
// 3.      Calculate Q = P - C, this is the new balance of your principal of your loan.
// 4.      Set P equal to Q and go back to Step 1: You thusly loop around until the value Q (and hence P) goes to zero.
// 

package exercises;

import java.lang.Math;
import java.lang.IllegalArgumentException;

public class AmortizationSchedule {

	private double amountBorrowed = 0; // in cents
	private double apr = 0d;
	private int initialTermMonths = 0;

	private double monthlyInterest = 0d;
	private long monthlyPaymentAmount = 0; // in cents

	/**
	 * Calculates monthly payment using formula M = P * (J / (1 - (Math.pow(1/(1
	 * + J), N)))); Where: P = Principal I = Interest J = Monthly Interest in
	 * decimal form: I / (12 * 100) N = Number of months of loan M = Monthly
	 * Payment Amount
	 * 
	 * @return Monthly payments to be made
	 */
	private void calculateMonthlyPayment() {

		// calculate J from annual percentage rate
		monthlyInterest = apr / (12d * 100d);

		// M = P * (J /
		// (1 - (Math.pow(1/(1 + J), N))));
		monthlyPaymentAmount = Math.round(amountBorrowed
				* (monthlyInterest / (1d - Math.pow(1 / (1d + monthlyInterest),
						initialTermMonths))));
	}

	/**
	 * Create the amortization schedule table The table is of the format: The
	 * first column identifies the payment number. The second column contains
	 * the amount of the payment. The third column shows the amount paid to
	 * interest. The fourth column has the current balance, the total payment
	 * amount and the interest paid fields.
	 */
	public void createAmortizationTable() {

		// Format to print the table's heading
		String formatHeading = "%-20s%-20s%-20s%s,%s,%s\n";
		// Format to print each line in the table
		String formatLine = "%-20d%-20.2f%-20.2f%.2f,%.2f,%.2f\n";

		AmortizationScheduleUtil.printf(formatHeading, "PaymentNumber",
				"PaymentAmount", "PaymentInterest", "CurrentBalance",
				"TotalPayments", "TotalInterestPaid");

		double principalBalance = amountBorrowed;
		double curBalance = amountBorrowed;
		int paymentNumber = 0;
		double totalPayments = 0;
		double totalInterestPaid = 0;
		double curMonthlyInterest = 0;
		double curPayoffAmount = 0;
		double curMonthlyPaymentAmount = 0;

		final int maxNumberOfPayments = initialTermMonths + 1;

		// iterate till the loan is paid and all the payments are done
		while ((principalBalance > 0) && (paymentNumber <= maxNumberOfPayments)) {

			// update principalBalance in each iteration
			// and make it equal to the current principal amount to be paid
			principalBalance = curBalance;

			// output is in dollars, so convert cents to dollars
			AmortizationScheduleUtil.printf(formatLine, paymentNumber++,
					curMonthlyPaymentAmount / 100d, curMonthlyInterest / 100d,
					curBalance / 100d, totalPayments / 100d,
					totalInterestPaid / 100d);

			// Calculate H = P x J, this is your current monthly interest
			curMonthlyInterest = Math.round(principalBalance * monthlyInterest);

			// current amount required to payoff the entire loan
			curPayoffAmount = principalBalance + curMonthlyInterest;

			// the amount to payoff the remaining balance may be less than the
			// calculated monthlyPaymentAmount
			curMonthlyPaymentAmount = Math.min(monthlyPaymentAmount,
					curPayoffAmount);

			// it's possible that the calculated monthlyPaymentAmount is 0,
			// or the monthly payment only covers the interest payment - i.e. no
			// principal
			// so the last payment needs to payoff the loan
			if ((paymentNumber == maxNumberOfPayments)
					&& ((curMonthlyPaymentAmount == 0) || (curMonthlyPaymentAmount == curMonthlyInterest))) {
				curMonthlyPaymentAmount = curPayoffAmount;
			}

			// Calculate C = M - H, this is your monthly payment minus your 
			//monthly interest, so it is the amount of principal you pay for that month
			double curMonthlyPrincipalPaid = curMonthlyPaymentAmount
					- curMonthlyInterest;

			// Calculate Q = P - C, this is the new balance of your principal of your loan.
			curBalance = principalBalance - curMonthlyPrincipalPaid;

			// update total amount and total interest paid till now
			totalPayments += curMonthlyPaymentAmount;
			totalInterestPaid += curMonthlyInterest;
		}
	}

	/**
	 * Instantiates a new amortization schedule object.
	 * 
	 * @param userInput
	 *            loan amount, interest rate and term entered by user
	 */
	public AmortizationSchedule(AmortizationScheduleInput userInput) {

		amountBorrowed = Math.round(userInput.getAmount() * 100); // converting
																	// dollars
																	// to cents
		apr = userInput.getApr();
		initialTermMonths = userInput.getYears() * 12;

		// calculate amount to be paid each month inclusive of interest in cents
		calculateMonthlyPayment();
	}

	public static void main(String[] args) {

		try {
			AmortizationScheduleInput amortizationInput = new AmortizationScheduleInput();
			amortizationInput.readInput();

			AmortizationSchedule amortizationSched = new AmortizationSchedule(
					amortizationInput);
			amortizationSched.createAmortizationTable();

		} catch (IllegalArgumentException e) {
			AmortizationScheduleUtil
					.print("Unable to process the values entered. Terminating program.\n");
		}
	}
}
