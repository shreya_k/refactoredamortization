package exercises;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.IllegalFormatException;

//Utility class for AmortizationSchedule. Contains methods for validation and console output.

public class AmortizationScheduleUtil {

	// valid range for amount borrowed
	private static final double[] borrowAmountRange = new double[] { 0.01d,
			1000000000000d };
	
	//valid range for Annual Percentage Rate (APR)
	private static final double[] aprRange = new double[] { 0.000001d, 100d };
	
	//valid range for the number of years for which the amount is borrowed 
	private static final int[] termRange = new int[] { 1, 1000000 };

	private static Console console = System.console();

	public static final double[] getBorrowAmountRange() {
		return borrowAmountRange;
	}

	public static final double[] getAPRRange() {
		return aprRange;
	}

	public static final int[] getTermRange() {
		return termRange;
	}

	/**
	 * Checks if the amount borrowed is within valid range
	 * @param amount the amount borrowed  
	 * @return true if valid, false if invalid
	 */
	public static boolean isValidBorrowAmount(double amount) {
		double range[] = getBorrowAmountRange();
		return ((range[0] <= amount) && (amount <= range[1]));
	}

	/**
	 * Checks if the Annual Percentage Rate (APR) is within valid range
	 * @param rate the annual interest rate 
	 * @return true if valid, false if invalid
	 */
	public static boolean isValidAPRValue(double rate) {
		double range[] = getAPRRange();
		return ((range[0] <= rate) && (rate <= range[1]));
	}
	
	/**
	 * Checks if the loan term is within valid range
	 * @param years the number of years for which the amount is borrowed 
	 * @return true if valid, false if invalid
	 */
	public static boolean isValidTerm(int years) {
		int range[] = getTermRange();
		return ((range[0] <= years) && (years <= range[1]));
	}

	/**
	 * Overrides the printf in java.io.PrintStream
	 *
	 * @param formatString string that specifies the formatting to be used
	 * @param args a list of the variables to be printed using that formatting
	 */
	public static void printf(String formatString, Object... args) {

		try {
			if (console != null) {
				console.printf(formatString, args);
			} else {
				System.out.format(formatString, args);
			}
		} catch (IllegalFormatException e) {
			System.err.print("Error printing...\n");
		}
	}

	/**
	 * Prints a string 
	 *
	 * @param s string to be printed 
	 */
	public static void print(String s) {
		printf("%s", s);
	}

	/**
	 * Reads a line of input from the user
	 *
	 * @param userPrompt prompt to be displayed to the user
	 * @return the string read from console 
	 * @throws IOException Signals if an I/O exception has occurred.
	 */
	public static String readLine(String userPrompt) throws IOException {
		String line = "";

		if (console != null) {
			line = console.readLine(userPrompt);
		} else {
			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(System.in));

			print(userPrompt);
			line = bufferedReader.readLine();
		}
		line.trim();
		return line;
	}
}
