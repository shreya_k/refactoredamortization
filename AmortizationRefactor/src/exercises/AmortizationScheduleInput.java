package exercises;

import java.io.IOException;

//input variables and methods for AmortizationSchdule

public class AmortizationScheduleInput {

	private double amount = 0;
	private double apr = 0;
	private int years = 0;

	public double getAmount() {
		return amount;
	}

	public double getApr() {
		return apr;
	}

	public int getYears() {
		return years;
	}


	/**
	 * Read and validate amortization input from the user via console
	 *
	 * @param userPrompt prompt to be displayed to user
	 */
	public void readInput() {

		//user prompts for the 3 input values required
		String[] userPrompts = {
				"Please enter the amount you would like to borrow: ",
				"Please enter the annual percentage rate used to repay the loan: ",
				"Please enter the term, in years, over which the loan is repaid: " };

		String inputLine = "";
		double amount = 0;
		double apr = 0;
		int years = 0;
		int i = 0;
		
		//iterate until user enters all the input values
		while (i < userPrompts.length) {
			String userPrompt = userPrompts[i];
			
			//display the prompt and try to read a line of input
			try {
				inputLine = AmortizationScheduleUtil.readLine(userPrompt);
			} catch (IOException e) {
				AmortizationScheduleUtil
						.print("An IOException was encountered. Terminating program.\n");
			}

			try {
				
				//in each case, parse the input line, check for validity of input and 
				//prompt the user to enter correct values if it is invalid
				
				switch (i) {
				
				//the first line of input is for amount borrowed
				case 0:
					amount = Double.parseDouble(inputLine);
					if (!AmortizationScheduleUtil.isValidBorrowAmount(amount)) {
						double range[] = AmortizationScheduleUtil
								.getBorrowAmountRange();
						AmortizationScheduleUtil
								.print("Please enter a positive value between "
										+ range[0] + " and " + range[1] + ". ");
					} else {
						this.amount = amount;
						i++;
					}
					break;

				//the second line of input is for annual percentage rate(apr)	
				case 1:
					apr = Double.parseDouble(inputLine);
					if (!AmortizationScheduleUtil.isValidAPRValue(apr)) {
						double range[] = AmortizationScheduleUtil.getAPRRange();
						AmortizationScheduleUtil
								.print("Please enter a positive value between "
										+ range[0] + " and " + range[1] + ". ");
					} else {
						this.apr = apr;
						i++;
					}
					break;

				//the third line of input is for the term of the loan in years
				case 2:
					years = Integer.parseInt(inputLine);
					if (!AmortizationScheduleUtil.isValidTerm(years)) {
						int range[] = AmortizationScheduleUtil.getTermRange();
						AmortizationScheduleUtil
								.print("Please enter a positive integer value between "
										+ range[0] + " and " + range[1] + ". ");
					} else {
						this.years = years;
						i++;
					}
					break;
				}
			//If any non-numeric data is entered then prompt user to enter correct data	
			} catch (NumberFormatException e) {
				AmortizationScheduleUtil.print("Please enter only numbers.");
			}
		}
	}

}
